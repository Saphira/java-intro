import java.util.Scanner;
public class LuckyNumber {
    public static void main(String[] args) {
        System.out.println("Enter a number from the range 0-9");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        if((number>9)||(number<0)){  
            System.out.println("You enter the number "+ number + ". Please enter a correct number from the range 0-9 ");
        }
        else if((number!=3) && (number!=5)) {
            System.out.println("Have a nice weekend");
        }
        else if(number==3){
            System.out.println("Your lucky number is 3");
        }
        System.out.println("Your lucky number is 5");
    }

}
