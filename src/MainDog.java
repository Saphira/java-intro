import oop.Dog;

import java.util.Scanner;

public class MainDog {
    public static void main(String[] args) {
        Dog dog = new Dog();
        System.out.println("Enter the age of the dog");
        Scanner input = new Scanner(System.in);
        dog.setAge(input.nextInt());
        System.out.println(dog.barkByAge(dog.getAge()));
        dog.barkByDefault();
    }

}
