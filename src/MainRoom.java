import oop.Room;

import java.util.Scanner;

public class MainRoom {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Room room = new Room();
        System.out.println("Please enter name");
        room.setName(input.next());
        //String name = input.next();
        System.out.println("Please enter height");
        room.setHeight(input.nextFloat());
        //float height = input.nextFloat();
        System.out.println("Please enter width");
        room.setWidth(input.nextFloat());
        //float width = input.nextFloat();
        System.out.println("Please enter length");
        room.setLength(input.nextFloat());
        //float length = input.nextFloat();
        //Room room = new Room(height, width, length, name);
        System.out.println(room.area(room.getLength(),room.getWidth()));
        System.out.println(room.volume(room.getLength(),room.getWidth(),room.getHeight()));
    }
}
