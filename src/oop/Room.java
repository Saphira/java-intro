package oop;

public class Room {
    private float height;
    private float width;
    private float length;

    public Room() {
    }

    private String name;

    public Room(float height, float width, float length, String name) {
        this.height = height;
        this.width = width;
        this.length = length;
        this.name = name;
    }

    public float area(float length, float width){
        return length*width;
    }

    public float volume(float length, float width, float height){
        return length*width*height;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
