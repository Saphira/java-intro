package oop;

public class Apple {
    private String colour;
    private int size;

    public Apple(int size, String colour) {
        this.colour = colour;
        this.size = size;
    }

    public Apple() {
        colour = "";
        size = 0;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String rot(int rotTime, String rotColour){
        while (rotTime > 0) {
            System.out.println("rotting...");
            --rotTime;
        }
        colour = rotColour;
        String text = "The apple is rotten.";
        return text ;
    }
}
