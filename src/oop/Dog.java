package oop;

public class Dog {
    private String name;
    private int age;
    private String color;
    private char sex;

    public Dog(String name, int age, String color, char sex) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.sex = sex;
    }

    public Dog(int age) {
        this.age = age;
    }

    public Dog() {
    }

    public void barkByDefault(){
        System.out.println("auh, auh, auh, auh");
    }
    public String barkByAge(int age){
        String barking;
        if (age<5)
            barking ="yip yip";
        else if(age>5&&age<10)
            barking ="arf arf ";
        else
            barking ="bork bork";
        return barking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }
}
